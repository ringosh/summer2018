#ifndef LIST_H
#define LIST_H
#include <iostream>
#include <string>

using namespace std;

struct ListItem
{
	string Value;
	ListItem* Next;
};

struct List
{
	ListItem* Head;
	ListItem* Tail;
};

void Init(List& L);
void Init(List& L, const string& InitValue);
void Init(List& L, const string InitValues[], const int N);
void Init(List& L, const List& Other);

void Prepend(List& L, const string& NewValue);
void Append(List& L, const string& NewValue);
ListItem* FindPrevious(const List& L, const ListItem *Item);
void InsertBefore(List& L, ListItem* CurrentItem, const string& NewValue);
void InsertAfter(List& L, ListItem* CurrentItem, const string& NewValue);

void RemoveFirst(List& L);
void RemoveLast(List& L);
void Remove(List& L, const string& ValueToRemove);
void RemoveAll(List& L, const string& ValueToRemove);
void Remove(List& L, const ListItem* ItemToRemove);
void Clear(List& L);

ListItem* Search(const List& L, const string& Value);
//ListItem* ReverseSearch(const List& L, const string& Value);

bool Contains(const List& L, const string& Value);

bool IsEmpty(const List& L);
int Count(const List& L);

void Report(const List& L);
void ReportStructure(const List& L);

void InternalRemove(List& L, const ListItem* ItemToDelete);

#endif // LIST_H
