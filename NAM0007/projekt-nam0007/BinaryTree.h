#pragma once
#include <iostream>
#include "Node.h"

using namespace std;

class BinaryTree
{
private:
	Node *root;
public:
	BinaryTree();
	BinaryTree(int value);
	BinaryTree(int *valueArray, unsigned int valueCount);
	~BinaryTree();

	void Add(int value);
	void Add(int *valueArray, unsigned int valueCount);

	void PrintRecursiveInorder(Node *node);
	void PrintRecursiveInorder();

	Node* Search(int value);

	bool IsEmpty();

	static bool CompareStructures(Node *a, Node *b);
	static bool CompareStructures(BinaryTree &a, BinaryTree &b);
};

